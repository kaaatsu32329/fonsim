"""
Test extrapolation of pvcurves
2021, February 4
"""

import fonsim as fons
import matplotlib.pyplot as plt
import numpy as np

filename = 'Measurement_60mm_balloon_actuator_01.csv'

pvc = fons.data.PVCurve(filename, extrapolate=False, autocorrect=True)

fig, ax = plt.subplots()
ax.plot(pvc.v*1e6, pvc.p*1e-5, label='datapoints')
ax.set_xlabel('volume [ml]')
ax.set_ylabel('pressure [bar]')
#plt.show()

pvc = fons.data.PVCurve(filename, extrapolate=False, autocorrect=True)
volumes = np.linspace(-10, 70, 100)*1e-6
pressures = np.array([pvc.fdf_volume(v)[0] for v in volumes])
ax.plot(volumes*1e6, pressures*1e-5, '--', label='extrapolated: False')
ax.legend()

pvc = fons.data.PVCurve(filename, extrapolate=True, autocorrect=True)
volumes = np.linspace(-10, 70, 100)*1e-6
pressures = np.array([pvc.fdf_volume(v)[0] for v in volumes])
ax.plot(volumes*1e6, pressures*1e-5, '-.', label='extrapolated: True')
ax.legend()

plt.show()
