=====
Tests
=====

This directory contains the tests.

Currently there are not yet many tests,
although it is the goal
that a decent test coverage will be reached
in the near future.

Test procedure
==============

The tests are organized
using the PyTest framework (docs.pytest.org).

To run all tests,
run ``pytest`` in terminal in this directory.
It will run all files
of the form 'test_*.py' and '*_test.py'.

