"""
Test tube flow
2021, February 4
"""

import matplotlib.pyplot as plt
import scipy as sp
import scipy.optimize as sp_opt
import numpy as np

import fonsim as fons

# Select fluid
fluid = fons.air

# Create tube
tube = fons.CircularTube(label='tube_01', fluid=fluid, diameter=0.2e-3, length=0.4)

# Evaluate flow over range of pressure drops
N = 100
p0 = fons.pressure_atmospheric
ps_a = p0 + np.zeros(N)
ps_b = p0 + np.linspace(-0.5e5, 2e5, N)
mfs = np.zeros(N)

for i in range(N):
    p_a = ps_a[i]
    p_b = ps_b[i]

    def f(mf):
        r = [0, 0]
        tube.evaluate(r, [], np.zeros((2, 4)), [], np.array([p_a, -mf[0], p_b, mf[0]]), 0)
        return r[0]**2 + r[1]**2
    mfs[i] = sp.optimize.minimize(f, [1]).x

rhos = fluid.rho_stp * np.divide(p0, (ps_a + ps_b)*0.5)
vfs = np.divide(mfs, rhos)

fig, axs = plt.subplots(2, 1)
axs[0].plot((ps_b-ps_a)*1e-5, mfs*1e3)
axs[0].set_ylabel('massflow [g/s]')
axs[1].plot((ps_b-ps_a)*1e-5, vfs*1e6)
axs[1].set_ylabel('volumeflow [ml/s]')
axs[1].set_xlabel('delta pressure [bar]')
plt.show(block=True)
