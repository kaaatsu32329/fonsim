"""
Validate the correctness
by comparing FONS' numerical solution
with an explicit analytical solution.
The system is piecewise linear.

2022, October 15
"""

import fonsim
import numpy as np
from fonsim.core import Variable, Terminal, Component, System, Simulation
from fonsim.components import PressureSource, Container
from fonsim.fluids.IdealCompressible import air
import fonsim.constants.norm as cnorm

import timeseries_comparison


terminal_fluidic = [Variable('pressure', 'across', cnorm.pressure_atmospheric),
                    Variable('massflow', 'through')]


class AvalancheValve(Component):
    """Opens when pa > pmax, closes when pa < pmin
    """
    def __init__(self, label=None, pmin=1.1e5, pmax=1.9e5):
        super().__init__(label)
        self.pmin, self.pmax = pmin, pmax
        ta = Terminal('a', terminal_fluidic)
        tb = Terminal('b', terminal_fluidic)
        self.set_terminals(ta, tb)
        self.set_arguments(ta('pressure'), ta('massflow'),
                           tb('pressure'), tb('massflow'))
        # angle == 0: closed (no fluid going through)
        # angle == 1: open (fluid freely going through)
        self.set_states(Variable('angle', 'local', 0))
        self.nb_equations = 2

    def evaluate(self, values, jacobian_state, jacobian_arguments, state, arguments, elapsed_time):
        if state[0] == 0:   # closed, massflow zero and pressures free
            values[:] = arguments[1], arguments[3]
            jacobian_arguments[0, 1] = 1
            jacobian_arguments[1, 3] = 1
        else:               # open, massflows complementary and pressures equal
            values[0] = arguments[1] + arguments[3]
            values[1] = arguments[0] - arguments[2]
            jacobian_arguments[0, 1] = 1
            jacobian_arguments[0, 3] = 1
            jacobian_arguments[1, 0] = 1
            jacobian_arguments[1, 2] = -1

    def update_state(self, state_new, jacobian, state, arguments, dt):
        if arguments[0] > self.pmax:
            state_new[0] = 1
        elif arguments[0] < self.pmin:
            state_new[0] = 0
        else:
            state_new[0] = state[0]


class Resistor(Component):
    """Linear flow restrictor
    """
    def __init__(self, label=None, resistance=1e9):
        super().__init__(label)
        self.r = resistance
        ta = Terminal('a', terminal_fluidic)
        tb = Terminal('b', terminal_fluidic)
        self.set_terminals(ta, tb)
        self.set_arguments(ta('pressure'), ta('massflow'),
                           tb('pressure'), tb('massflow'))
        self.nb_equations = 2

    def evaluate(self, values, jacobian_state, jacobian_arguments, state, arguments, elapsed_time):
        # pressure drop
        values[0] = arguments[0] - arguments[2] - self.r * arguments[1]
        # continuity of massflow
        values[1] = arguments[1] + arguments[3]
        jacobian_arguments[0, 0] = 1
        jacobian_arguments[0, 1] = -self.r
        jacobian_arguments[0, 2] = -1
        jacobian_arguments[1, 1] = 1
        jacobian_arguments[1, 3] = 1


def test(r1=1e8, r2=1e7, p1=2e5, p2=1e5, pmin=1.1e5, pmax=1.9e5, v=100e-6,
         duration=1.0, step=1e-3,
         visualize=False):
    # Numerical simulation using FONS
    sys = System()
    sys += PressureSource('source_high', fluid=air, pressure=p1)
    sys += PressureSource('source_low', fluid=air, pressure=p2)
    sys += AvalancheValve('valve', pmin=pmin, pmax=pmax)
    c = Container('container', fluid=air, volume=v)
    c.states[0].initial_value = \
        c.volume * air.rho_stp * pmin / cnorm.pressure_atmospheric
    sys += c
    sys += Resistor('R1', resistance=r1)
    sys += Resistor('R2', resistance=r2)

    sys.connect('source_high', 'R1')
    sys.connect('R1', 'container')
    sys.connect('container', 'valve')
    sys.connect('valve', 'R2')
    sys.connect('R2', 'source_low')

    sim = Simulation(sys, duration=duration, step_min=step, step_max=step)
    sim.run()

    # Analytical solution
    t = sim.times
    p_stp = cnorm.pressure_atmospheric
    rho = air.rho_stp
    pbis = (p1 / r1 + p2 / r2) / (1/r1 + 1/r2)
    # Linear system piecewise time constants
    tau_open = 1 / (p_stp / (rho * v * r1))
    tau_closed = 1 / (p_stp / (rho * v) * (1/r1 + 1/r2))
    # and their piecewise explicit functions
    def p_open(t): return np.exp(-t / tau_open) * (pmin - p1) + p1
    def p_closed(t): return np.exp(-t / tau_closed) * (pmax - pbis) + pbis

    # Combined piecewise pressure function
    def p(t):
        # Pieces durations
        dt_open = -np.log((pmax - p1) / (pmin - p1)) * tau_open
        dt_closed = -np.log((pmin - pbis) / (pmax - pbis)) * tau_closed
        # Resolve periodicity
        t = t % (dt_open + dt_closed)
        # Piecewise
        def f(ti): return p_open(ti) if ti < dt_open else p_closed(ti - dt_open)
        f = np.vectorize(f)
        # Evaluate
        return f(t)

    if visualize:
        # Plot results
        import matplotlib.pyplot as plt
        from fonsim.visual.plotting import plot
        fig, axs = plt.subplots(2, sharex=True, constrained_layout=True)
        plot(axs[0], sim, label='pressure', unit='bar',
             components=['source_high', 'container', 'source_low'])
        axs[0].plot(t, p(t) * 1e-5, '--')
        plot(axs[1], sim, label='massflow', unit='g/s',
             components=[('R1', 'a'), ('R2', 'a')])
        axs[-1].set_xlabel('time [s]')
        plt.show(block=True)

    # Compare results
    p_analytic = p(t)
    assert timeseries_comparison.isclose_2d(
        t[1:], p_analytic[1:], sim.times[1:], sys.get('container').get('pressure')[1:],
        x_rtol=5e-2, x_atol=1e-3, y_rtol=0.0, y_atol=1e-2*2e5)


if __name__ == '__main__':
    test(visualize=True)
