print('=== FONSIM 0.3 ===')
print('NOTICE: The FONSim package currently resides in the beta stage. '
      'Some features do not work fully yet and you may encounter bugs. '
      'Several features and implementations are still in development '
      'and will change in the coming months. '
      'Therefore it is suggested, for later reference, '
      'to note in your script the used FONSim version. '
      'We look forward to your feedback, and thank you for your understanding. '
      )
print()
