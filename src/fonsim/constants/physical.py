"""
Some physical constants and parameters, such as the gas constant
Please refer to the source of this file
to see the defined constants.

2020, July 21
"""

# gas constant [J / (K mol)]
gas_constant = 8.314459848
