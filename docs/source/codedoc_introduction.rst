============
Introduction
============

Introduction to codedoc.

The codedoc has been generated using Sphinx from the docstrings in the code.

The text below
provides a high-abstraction-level introduction
to the FONSim codebase.

Main parts
==========
FONSim consists of nine main parts.
Each part is discussed briefly.

- ``core``:
  This is the network simulation functionality.
  It is written agnostically of the domain (fluidic, eletric, ...) to be simulated.
  This core is discussed in detail further on.
- ``components``:
  The standard library of fluidic components
  with actuators, tubes, restrictors, sources etc.
- ``wave``:
  Signal generators: block wave, sine wave etc.
- ``data``:
  Functionality for handling data.
  The files ``curve.py``, ``pvcurve.py``, ``dataseries.py`` and ``interpolate.py``
  ease working with pv-curves, including reading them in from CSV files.
  The simulation data export functionality resides in ``writeout.py``.
- ``fluid``:
  Defines how fluids should be specified
  in the form of class definitions.
  Currently supported are
  ideal gasses and incompressible liquids.
- ``fluids``:
  Standard library of fluids with air, water etc.
- ``visual``:
  Plotting functionality specific to visualizing simulation results.
- ``conversion``:
  String comparison and unit conversion.
- ``constants``:
  Physical constants.
- ``resources``:
  Data files such as pv-curves in CSV format,
  mostly used in examples and tests.


Core
====
The core contains all functionality that is required
for simulating networked system
but, as it is written domain-agnostic,
it does not contain any code specific to fluidic system.

The core consists of seven files, each discussed here.

- ``variable.py``

  Class definition of ``Variable``.
  Variable objects are used to tell to the solver
  that there is a yet unknown timeseries
  of numerical scalar values
  that will have to be solved for during the simulation.
  After running the simulation, for each Variable object
  a 1D array of values will have been derived
  that satisfies the given equations best.
  To put it simply,
  the solver uses the Variable objects
  to keep reference of what numerical value
  belongs to what equations.

  A Variable has a key to indicate the medium it refers to
  (for example, pressure, massflow, or temperature).
  It also has an orientation to indicate
  which network equations should be applied
  when it is connected to other components
  (or when it is used stand-alone).

- ``terminal.py``

  Class definition of ``Terminal``.
  A Terminal contains one or more Variable instances
  that are often used alongside each other.
  A Terminal can be connected to another terminal.
  The solver automatically generates the network equations
  (which relates the Variables of different components to each other)
  from how the terminals are connected to each other.
  Therefore, the Variables in a Terminal should be defined such
  that they make physical sense.

  In practice this often leads to two Variables,
  one with orientation *across*
  and another with orientation *over*.
  Examples of typical pairs and their domain
  include pressure and massflow (fluidic),
  voltage and current (electric),
  linear displacement and force (mechanical, linear),
  rotational displacement and torque (mechanical, rotational).

- ``component.py``

  Class definition of the base class ``Component``.
  This class definition provides the functionality
  common to all components.
  Usable components such as actuators, sources etc.
  inherit from this base class
  and add the relevant Variables, Terminals
  and internal equations.
  In practice,
  the class Component is never instantiated,
  as doing so would create a component
  without any physical usefulness.

- ``system.py`` and ``node.py``

  Class definitions of respectively ``System`` and ``Node``.
  A System object holds information
  on which components it contains
  and how they are connected to each other.
  A connection between two terminals
  (a connection is always between terminals,
  not between components)
  is recorded in a Node object.
  A Node class instance contains of data
  only a list of Terminals it is connected to,
  the Node class mostly defines functionality
  to manipulate Node objects.

  While it is usual
  to say that x terminals are connected to each other,
  in the simulator this is handled
  by connecting those x terminals
  to a common node.
  Each Node instance therefore contains the Terminals
  the Node is connected to.
  A Terminal is always connected to a Node,
  and vice-versa.
  A Terminal may not be connected
  to more than one Node.
  A Terminal does not know to which Node it is connected,
  this information is only stored in the nodes.

- ``simulation.py``

  Class definition of Simulation.
  This class provides functionality for:

  - Deriving mathematical constructs
    from a relationally-specified System object:
    matrices, listing all equations etc.
  - Evaluating all component equations
  - Memory management
  - Starting the the numerical simulation

  For solving the resulting system of equations
  it uses a solver.
  The solvers are defined in ``solver.py``.

  Putting the relationally-specified System object
  in a mathematical form is a computationally intensive task
  and is done only once during the initialization
  thanks to a cache system.

- ``solver.py``

  Numerical solvers.
  These solvers assume particular functionality
  of the class Simulation.

