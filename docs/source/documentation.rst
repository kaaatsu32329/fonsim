=============
Documentation
=============


Introduction
============
The documentation of the FONSim project resides
together with the code in the GitLab repository
in the directory *docs/source*.
This directory contains the documentation source files,
written in reStructuredText (rST).
All rST files in the directory *autodoc* are generated using tools
while the other rST files, such as the one used to render this document,
are compiled manually.

These reStructuredText files are rendered to HTML,
viewable in a browser, using Sphinx.
This HTML documentation is then hosted on the internet
thanks to the ReadTheDocs organization.
The ReadTheDocs servers look at the FONSim GitLab repository
and more or less rebuilds the documentation every time
the codebase on the GitLab repository changes.

The Sphinx + ReadTheDocs documentation has a nice
`"How To" guide <https://sphinx-rtd-tutorial.readthedocs.io/en/latest/index.html>`_.
The FONSim project mostly adheres to the default choices.

The rest of this documents discusses how to build the documentation.


Local compilation
=================

The output HTML files
reside in ``docs/build/``
and can be viewed in a web browser.
They closely resemble those that will be built by the ReadTheDocs server
(their process is slightly different though).

First time:
::

   cd docs
   sphinx-build -b html source/ build/

Thereafter:
::

   cd docs
   make clean
   make html

The ``make clean`` is preferable not omitted
as doing so tends to lead to not updating files that should have been updated.

Compiling autodocs
==================

In short,
the Sphinx autodocs functionality takes the codebase,
strips all the code away and dresses the docstrings
in nice rST files.
These rST files only contain references to the docstrings
and therefore not the docstrings themselves.
Therefore, it is not necessary to rerun the Sphinx autodocs
unless major changes to the code structure are made.
The generated autodocs reside in the directory ``docs/source/autodocs``.

::

   cd docs
   sphinx-apidoc -o ./source/autodoc ../src/fonsim -f
   rm source/autodoc/modules.rst
   rm source/autodoc/fonsim.rst

The files ``modules.rst`` and  ``fonsim.rst`` are deleted
to avoid generating
'WARNING: document isn't included in any toctree'.


Updating ReadTheDocs
====================

The ReadTheDocs server is configured to look at the GitLab repository
and recompiles the documentation automatically.
However, it will fail at the smallest error,
so it is preferable to check the documentation source files first
by doing a local compilation and observing the output.

