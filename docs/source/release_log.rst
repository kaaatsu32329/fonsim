===========
Release log
===========


Upcoming releases
=================

0.4: Late spring 2023
---------------------
Changes
 - Introduce the vastly improved solver.
   Currently (Spring 2023) resolving
   some minor issues
   and adding tests.


Previous releases
=================
0.3: 2023, April 03
-------------------
The first beta release!
FONSim appears to work well and no serious issues did arise,
therefore change from alpha to beta.
Extended and cleaned the documentation,
only minor code changes.

Changes
 - Extend and clean documentation.
 - Point to the new [fonsim.org](https://fonsim.org) domain name
   (URL and email addresses).
 - Introduce Binder to try-out FONSim easily.
 - Scipy interpolation bugfix.


0.2a: 2023, February 04
-----------------------
The second release of FONSim
focuses on cleaning up the codebase,
improving documentation to make the codebase
attractive to new users,
and the introduction
of the *autocall* and *autodiff* functionality.

Changes
 - Introduce autocall:
   in component definition,
   reference variables and states by their label,
   removing the requirement to work with arrays and indices.
 - Introduce autodiff:
   in component definition, calculate derivatives
   automatically using numerical differentiation,
   removing the requirement to specify them manually.
 - Introduce more than 100 tests (PyTest framework)
   covering most of FONSim's codebase.
 - Documentation (tutorials + formal codedoc) on readthedocs
   (went online spring 2022).
 - Many bugfixes and code, UI and documentation improvements.
   Some breaking changes were made.


0.1a5: 2021, March 24
---------------------

Initial release of FONSim on PyPI.
