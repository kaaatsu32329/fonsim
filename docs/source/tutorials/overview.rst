============
Introduction
============

Here you find a set of tutorials
aimed at introducing newcomers to FONSim.

Available tutorials
::

  2.1    Installing FONSim
  2.2    A first network
  2.3    A more complex network
  2.5    Balloon actuators: exploiting bistability
  2.x    Custom components


Planned tutorials
::

  2.7    Simulation data viewing
  2.8    Solver options
  2.9    More complex systems
  2.11   A refrigerator


In case you have any thoughts, ideas, ...
which would improve these tutorials,
sharing them would be greatly appreciated.