=================
Balloon actuators
=================

The previous two examples already showed how to use several main features of FONSim,
however, the observed behaviour of the container components was rather simple.
Balloon actuators provide a more interesting component
and are studied extensively in the field of soft robotics (SoRo).
One of their special properties is their nonlinear behaviour.
On one hand this makes them somewhat difficult to use for existing applications,
but it also opens a wide array of possibilities for new applications
such as hardware-encoded sequencing.

PV curves
=========

The behaviour of these balloon actuators is specified as a pressure-volume curve
or pv-curve.
This curve is usually derived from measurements.
FONSim contains an example measurements file of a balloon actuator:
:download:`Measurement_60mm_balloon_actuator_01.csv
<../../../src/fonsim/resources/Measurement_60mm_balloon_actuator_01.csv>`.
This particular example curve was measured as part of the research for
`this publication
<https://www.researchgate.net/publication/329109975_Hardware_Sequencing_of_Inflatable_Nonlinear_Actuators_for_Autonomous_Soft_Robots/>`_.

Let's have a look at the first five lines of this CSV file:

.. literalinclude:: ../../../src/fonsim/resources/Measurement_60mm_balloon_actuator_01.csv
   :lines: 1-5
   :lineno-match:

The file is split in four parts
(explained more in depth in :py:class:`.PVCurve`):

- First line: information about the file.
  For example, the measurement occurred in 2017, November 16.
- Second line: keys of data columns.
  Here we're only interested in volume and pressure,
  though time is listed as well.
- Third line: units of data columns.
- Fourth line and further: the actual numerical data.

Lets read this file using FONSim and plot the curve.
First, the CSV file is loaded into a :py:class:`.PVCurve` object.
The resulting object has two attributes, ``v`` and ``p``,
which respectively are 1D Numpy arrays of volume and pressures.
Their units have been automatically adjusted such they conform to SI base units.
The pressure has also been adjusted to absolute
because the measured pressure in the CSV file
was relative to atmospheric pressure.

.. literalinclude:: ../../../examples/06.py
   :start-at: # Load pvcurve
   :end-at: plt.show
   :lineno-match:

Full file: :download:`06.py <../../../examples/06.py>`.
The plotting result:

.. figure:: Figure_03.png

   Figure 1: PV curve of a balloon actuator

Figure 1 shows what makes these balloon actuators so interesting:
the non-monotonically increasing pressure.
As a result, for some pressures there are multiple solutions
for the volume of the actuator, three in this case.
Note the local pressure maximum of 1.87 bar
around a volume of 6 ml (the 'hill')
and a local pressure minimum of 1.58 bar
around a volume of 21 ml (the 'dip').


Single balloon actuator
=======================

Let's do a simulation with this balloon actuator!
Full file: :download:`04.py <../../../examples/04.py>`.

After the usual preamble,
the pressure reference and the fluid are selected:

.. literalinclude:: ../../../examples/04.py
   :start-at: # Pressure reference
   :end-at: fluid =
   :lineno-match:

The previous required a compressible fluid
as all components were constant-volume.
In this example, the balloon actuators can vary their volume,
so feel free to try with an incompressible fluid (for example, water)!

====

Next, the system is built and the simulation is run.
The system consists of one pressure source,
one balloon actuator (FreeActuator)
and a tube connecting the former two.

The FreeActuator component allows to simulate components characterized by a pv-curve
and therefore provides an excellent fit for simulating balloon actuators.
If the pvcurve is available as a CSV file,
the first two emphasized lines can be omitted
and the curve filepath can be passed directly to the parameter ``curve``
of :py:class:`.FreeActuator`.

.. literalinclude:: ../../../examples/04.py
   :start-at: # Create system
   :end-at: sim.run
   :lineno-match:
   :emphasize-lines: 6-8


====

Let's plot the simulation results!
Like in the previous example,
we'll use the FONSim built-in plotting functionality:

.. literalinclude:: ../../../examples/04.py
   :start-at: # Plot
   :end-at: plt.show
   :lineno-match:

This outputs the following plot figure:

.. figure:: Figure_04.png

   Figure 2: Simulation output

====

To finish, a brief discussion of the simulation results
shown in Figure 2.
The simulation starts
with an increase of absolute pressure to 1.89 bar,
the actuator fully inflates
as this pressure is larger than
the pvcurve hill.
Shortly after the pvcurve has been fully inflated,
the pressure decreases back to 1.65 bar,
just above the pvcurve dip.
The actuator therefore stays mostly inflated,
as is evidenced by more than half of the mass
remaining, shown in the middle graph of Figure 2.

At time = 2.0 s the pressure drops just 0.1 bar,
to 1.55 bar.
Nevertheless, the actuator now deflates almost fully.
Increasing the pressure again at time = 3.0 s
has little effect on the actuator volume
(middle graph of Figure 2).


Two balloon actuators
=====================

In this second simulation,
we'll look at two balloon actuators in series
and show a basic example of hardware sequencing achieved
by exploiting a combination of actuator nonlinearity and flow restriction.
It is based on TODO paper reference.
The code is very similar to the previous simulation,
so only the relevant differences are discussed here.
Full file: :download:`07.py <../../../examples/07.py>`.

Pressure reference:

.. literalinclude:: ../../../examples/07.py
   :start-at: # Pressure reference
   :end-at: + fons.pressure
   :lineno-match:

tubes:

.. literalinclude:: ../../../examples/07.py
   :start-at: tube_0
   :end-at: tube_1

and the plotting:

.. literalinclude:: ../../../examples/07.py
   :start-at: # Plot simulation
   :end-at: plt.show()

This gives the following two figures:

.. figure:: Figure_05.png

   Figure 3: Simulation output

.. figure:: Figure_06.png

   Figure 4: Simulation output, actuator volumes

====

Figures 3 and 4 show the simulation results.
The middle graph of Figure 3
shows the achieved phase lag.
Even while actuator 0 is inflated first,
it is actuator 1 that is deflated last.
Also note that the system state between the transitions is stable,
as is shown by the constant pressure and mass in these region.
These regions can thus be elongated or shortened
by changing the pressure reference timings.

This phase lag is also well visible in Figure 4,
which plots a virtual path traversed by the actuator volumes.
In the research work
`Hardware Sequencing of Inflatable Nonlinear Actuators for Autonomous Soft Robots
<https://www.researchgate.net/publication/329109975_Hardware_Sequencing_of_Inflatable_Nonlinear_Actuators_for_Autonomous_Soft_Robots/>`_,
this phase lag was used to produce a stepping motion
for a legged robot,
where one actuator moved the feet horizontally
and another one moved the feet vertically.
