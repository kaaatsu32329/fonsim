fonsim.fluids package
=====================

Submodules
----------

fonsim.fluids.Bingham module
----------------------------

.. automodule:: fonsim.fluids.Bingham
   :members:
   :undoc-members:
   :show-inheritance:

fonsim.fluids.IdealCompressible module
--------------------------------------

.. automodule:: fonsim.fluids.IdealCompressible
   :members:
   :undoc-members:
   :show-inheritance:

fonsim.fluids.IdealIncompressible module
----------------------------------------

.. automodule:: fonsim.fluids.IdealIncompressible
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: fonsim.fluids
   :members:
   :undoc-members:
   :show-inheritance:
