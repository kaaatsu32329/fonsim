.. FONSim documentation master file, created by
   sphinx-quickstart on Tue Apr 19 14:49:26 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

==================================
Welcome to FONSim's documentation!
==================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

.. toctree::
   :hidden:
   :caption: The FONSim Project
   :maxdepth: 1

   introduction
   working_principles
   development
   release_log

.. toctree::
   :hidden:
   :caption: Tutorials
   :maxdepth: 1

   tutorials/overview
   tutorials/installing
   tutorials/a_first_network
   tutorials/a_more_complex_network
   tutorials/balloon_actuators
   tutorials/custom_components

.. toctree::
   :hidden:
   :caption: Codedocs
   :maxdepth: 2

   codedoc_introduction
   autodoc/fonsim.core
   autodoc/fonsim.components
   autodoc/fonsim.data
   autodoc/fonsim.fluid
   autodoc/fonsim.fluids
   autodoc/fonsim.wave
   autodoc/fonsim.visual
   autodoc/fonsim.constants
   autodoc/fonsim.conversion



Go to the :doc:`introduction` page.


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
