# FONSim
#### _Fluidic Object-oriented Network SIMulator_

An object-oriented Python 3 library designed for simulating pneumatic and hydraulic systems in soft robots.
Find the full documentation on [fonsim.org](https://fonsim.org).

This project is available under the GNU Affero General Public License v3.0 (**agpl-3.0**) license.

### Installation
The pre-packaged release version can be installed using `pip install fonsim`.
See the [PyPi page](https://pypi.org/project/fonsim/) for more information.

Currently, FONSim resides in the beta stage.
Some features do not work well yet, or are unintuitive to use correctly,
and you may encounter bugs.
We look forward to your feedback.


### How to get started
The [examples directory](
  https://gitlab.com/abaeyens/fonsim/-/tree/master/examples)
contains a set of examples showcasing various features of the simulator.
We suggest to start with running the examples.
Furthermore you may want to consult the documentation on
[readthedocs.org](https://fonsim.readthedocs.io/en/feature-doc/introduction.html).
The code documentation is also available by the
[Python help function](https://www.programiz.com/python-programming/docstrings#help).
If something is not fully clear, please let us know.

### Key dependencies
* matplotlib
* numpy
* scipy


## Project development, contribution

### Contributing
Are you interested in contributing to this project?
Please get in touch so we can coordinate the development!

### Contributors
Core developers:  
Arne Baeyens  
Bert Van Raemdonck  

As well as many thanks to:  
Edoardo Milana  
Benjamin Gorissen  


## Problems, questions, suggestions
If you have a question the FAQ section does not answer sufficiently,
or you think you have encountered a bug,
you can reach out by creating an issue on the
[GitLab Issues](https://gitlab.com/abaeyens/fonsim/-/issues) page.
The issues page is also the proper place to post suggestions and feedback.


## FAQ
Please refer to the documentation on 
[readthedocs.org](https://fonsim.readthedocs.io/en/feature-doc/introduction.html).
