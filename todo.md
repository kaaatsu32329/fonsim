# Todo
A list of todo's

2020, August 31

## List
- streamline pv-curve reading functionality
  - **COMPLETED** 2020, September 5
  - currently relies on _relation.py_ and _dataseries.py_.
    Neither of those are implemented well,
    so best to replace them both.
- improve sequence generator
  - **COMPLETED** 2020, September 5
  - function *step_function* in _signals.py_
  - renamed to _waves.py_ and _customwave.py_
- mathematical models for all components
  - **COMPLETED** 2020, September 16
  - basic set of components
  - compressible and incompressible flow
- documentation
  - Sphinx: https://packaging.python.org/tutorials/creating-documentation/
  - maybe host in on https://readthedocs.org/?
- update plotting interface
  - ease getting simulation data from components
    - **COMPLETED** 2020, September 5
  - easy-plotting function
    - **COMPLETED** 2020, September 5
    - see _plotting.py_
- save simulation data to file
  - format: JSON
    - **COMPLETED** 2020, September 9
- fluidic port definition
  - **COMPLETED** 2020, September 10
  - currently two ports for fluids:
    one for compressible (pneumatics) and one for incompressible (hydraulics)
  - merge to one and leave it to the components to work out
    the appropriate equations
- update fluids class
  - **COMPLETED** 2020, September 14
  - hierarchical structure for simple expansion to more complex fluids
- component equations depending on fluid
  - **COMPLETED** 2020, September 14
  - used equations should depend on fluid
- reorganize components in separate files
  - **COMPLETED** 2020, September 10
- component initial state
  - **COMPLETED** 2020, September 10
  - component states currently start out with zeros
- arguments/variables initial value
  - arguments (pressure etc.) initialized to zero can give start-up problems,
    especially in combination with compressible fluids.
- update examples
- package files for PyPi distribution
  - **in progress**
  - https://packaging.python.org/tutorials/packaging-projects/
  - https://docs.python-guide.org/writing/structure/
  - main effort: restructuring files
  - move data files (e.g. default pv curve) to `data` directory
    - **COMPLETED** 2020, September 18
    - some problems with default files; _example_05.py_ currently can't find default file
      - issue: reading package data files interface different from reading 'normal' files
