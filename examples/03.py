"""
Example 03
Based on: 02
Node: two containers
FONS plotting functionality

         -=- container
src -=- |
         -=- container

2020, July 22
"""

import fonsim as fons
import matplotlib.pyplot as plt

# Create wave function for pressure source
waves = [(0, 0.900), (0.5, 0.100), (1.0, 0.500)]
wave_function = fons.wave.Custom(waves)*1e5\
                + fons.pressure_atmospheric

# Create system
system = fons.System()
# Select fluid
fluid = fons.air
# Create components and add them to the system
system.add(fons.PressureSource('source', pressure=wave_function))
system.add(fons.Container('container_0', fluid=fluid, volume=100e-6))
system.add(fons.Container('container_1', fluid=fluid, volume=100e-6))
system.add(fons.CircularTube('tube_0', fluid=fluid, diameter=2e-3, length=0.10))
system.add(fons.CircularTube('tube_1', fluid=fluid, diameter=2e-3, length=0.02))
system.add(fons.CircularTube('tube_2', fluid=fluid, diameter=2e-3, length=0.70))
# Connect components to each other
system.connect(('tube_0', 'a'), 'source')
system.connect(('tube_0', 'b'), ('tube_1', 'a'))
system.connect(('tube_0', 'b'), ('tube_2', 'a'))
system.connect('tube_1', 'container_0')
system.connect('tube_2', 'container_1')

# Create simulation and run it
sim = fons.Simulation(system, duration=1.5)
sim.run()

# Plot results
fig, axs = plt.subplots(3, sharex=True)
fig.suptitle("Simulation results")
fons.plot(axs[0], sim, 'pressure', 'bar', ('source', 'container_0', 'container_1'))
fons.plot_state(axs[1], sim, 'mass', 'g', ('container_0', 'container_1'))
fons.plot(axs[2], sim, 'massflow', 'g/s', ('tube_0', 'tube_1', ('tube_2', 'a')))
plt.show()
