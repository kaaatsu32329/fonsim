{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "3c8aede7",
   "metadata": {},
   "source": [
    "# FONSim introductory example\n",
    "This example shows a simulation\n",
    "of a pneumatic network popular in soft robotics research\n",
    "that consists of a pressure source\n",
    "and two balloon actuators with identical pvcurves in series,\n",
    "with a restrictor (thin tube) interconnecting the two actuators.\n",
    "\n",
    "As the simulation results will show, the inflation sequence\n",
    "of the two actuators show a stable phase lag.\n",
    "Such a movement sequence can be used to drive legged robots.\n",
    "\n",
    "System schematic:\n",
    "```\n",
    "src -=- act -=- act\n",
    "```\n",
    "\n",
    "For a more in-depth discussion\n",
    "and a more gradual introduction to the many features\n",
    "FONSim provides,\n",
    "please refer to [this tutorial](\n",
    "  https://fonsim.readthedocs.io/en/latest/tutorials/balloon_actuators.html)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "6118b594",
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib inline\n",
    "import fonsim as fons\n",
    "import matplotlib.pyplot as plt\n",
    "import importlib_resources as ir"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b1f8ee52",
   "metadata": {},
   "source": [
    "## The PV-curve"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "2acd11ea",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Load the PV-curve from a CSV file\n",
    "filepath = 'resources/Measurement_60mm_balloon_actuator_01.csv'\n",
    "ppath = str(ir.files('fonsim').joinpath(filepath))\n",
    "pvcurve = fons.data.PVCurve(ppath)\n",
    "\n",
    "# Plot the PV-curve\n",
    "fig, ax = plt.subplots(1)\n",
    "ax.plot(pvcurve.v * 1e6, pvcurve.p * 1e-5)\n",
    "ax.set_ylabel('absolute pressure [bar]')\n",
    "ax.set_xlabel('volume [ml]')\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4fb0706b",
   "metadata": {},
   "source": [
    "## Build the fluidic network"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d502bdbd-bdbf-4694-9c18-1a2aabacc0ac",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Pressure reference wave function\n",
    "p1 = 0.890\n",
    "p2 = 0.650\n",
    "p3 = 0.200\n",
    "waves = [(0.0, p2),\n",
    "         (1.0, p1), (1.3, p2),\n",
    "         (3.0, p1), (3.5, p2),\n",
    "         (5.0, p3), (5.3, p2),\n",
    "         (7.0, p3), (7.4, p2)]\n",
    "wave_function = fons.wave.Custom(waves, time_notation='absolute')*1e5\\\n",
    "                + fons.pressure_atmospheric\n",
    "\n",
    "# Select a fluid\n",
    "fluid = fons.air\n",
    "\n",
    "# Create system\n",
    "system = fons.System()\n",
    "system.add(fons.PressureSource('source', pressure=wave_function))\n",
    "system.add(fons.FreeActuator('actu_0', fluid=fluid, curve=pvcurve))\n",
    "system.add(fons.FreeActuator('actu_1', fluid=fluid, curve=pvcurve))\n",
    "system.add(fons.CircularTube('tube_0', fluid=fluid, diameter=2e-3, length=1.20))\n",
    "system.add(fons.CircularTube('tube_1', fluid=fluid, diameter=2e-3, length=0.60))\n",
    "system.connect('source', 'tube_0')\n",
    "system.connect('tube_0', 'actu_0')\n",
    "system.connect('actu_0', 'tube_1')\n",
    "system.connect('tube_1', 'actu_1')\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0c78b385",
   "metadata": {},
   "source": [
    "## Create and run the simulation\n",
    "\n",
    "The normal simulation calculation duration\n",
    "on a mid-end laptop is about 6 s.\n",
    "If you're running this simulation in a Binder instance,\n",
    "the compute resources are limited\n",
    "and you can expect considerably longer durations."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c0c10a51",
   "metadata": {},
   "outputs": [],
   "source": [
    "sim = fons.Simulation(system, duration=9, step_max=1e-2)\n",
    "sim.run()\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5ec08b5a",
   "metadata": {},
   "source": [
    "## Visualize the simulation results"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "18fbfec0",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Time graph\n",
    "fig, axs = plt.subplots(3, sharex=True)\n",
    "fons.plot(axs[0], sim, 'pressure', 'bar', ('source', 'actu_0', 'actu_1'))\n",
    "axs[0].set_ylim(1.1, 2.0)\n",
    "fons.plot_state(axs[1], sim, 'mass', 'g', ['actu_0', 'actu_1'])\n",
    "fons.plot(axs[2], sim, 'massflow', 'g/s', ['tube_0', 'tube_1'])\n",
    "axs[2].set_xlabel('time [s]')\n",
    "for a in axs: a.legend()\n",
    "plt.show(block=False)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "08a36c51",
   "metadata": {},
   "source": [
    "Of particular interest is that in between the pressure source variations\n",
    "(for example, around time 2.0 s, 4.2 s and 6.2 s),\n",
    "the system converges to an equilibrium.\n",
    "This equilibrium can be stretched in time arbitrarily\n",
    "(feel free to try out yourself\n",
    "by playing with the pressure source wave function!).\n",
    "Hence the phase lag is considered to be stable."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b2de4f3b",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Also do a parametric plot\n",
    "rho = fluid.rho if hasattr(fluid, 'rho') else fluid.rho_stp\n",
    "p_atm = fons.pressure_atmospheric\n",
    "m_a0 = system.get('actu_0').get_state('mass')\n",
    "p_a0 = system.get('actu_0').get('pressure')\n",
    "v_a0 = m_a0 / rho * p_atm / p_a0\n",
    "m_a1 = system.get('actu_1').get_state('mass')\n",
    "p_a1 = system.get('actu_1').get('pressure')\n",
    "v_a1 = m_a1 / rho * p_atm / p_a1\n",
    "\n",
    "fig, ax = plt.subplots(1)\n",
    "ax.plot(v_a0 * 1e6, v_a1 * 1e6)\n",
    "ax.set_aspect('equal')\n",
    "ax.set_xlabel('volume actuator 0 [ml]')\n",
    "ax.set_ylabel('volume actuator 1 [ml]')\n",
    "plt.show(block=False)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d6b18a99",
   "metadata": {},
   "source": [
    "While not visualized explicitly in the figures above,\n",
    "the actuator's deformation is almost directly related\n",
    "to its volume. Therefore, the defomation is not visualized."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.10"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
