"""
Example 07
Based on: 04
A soft robotics balloon actuator with pvcurves.
Two balloon actuators in series.

src -=- act -=- act

2020, July 22
"""

import fonsim as fons
import matplotlib.pyplot as plt
import importlib_resources as ir

# Pressure reference
p1 = 0.890
p2 = 0.650
p3 = 0.200
waves = [(0.0, p2),
         (1.0, p1), (1.3, p2),
         (3.0, p1), (3.5, p2),
         (5.0, p3), (5.3, p2),
         (7.0, p3), (7.4, p2)]
wave_function = fons.wave.Custom(waves, time_notation='absolute')*1e5\
                + fons.pressure_atmospheric

# Select a fluid
fluid = fons.air

# Create system
system = fons.System()
system.add(fons.PressureSource('source', pressure=wave_function))
filepath = 'resources/Measurement_60mm_balloon_actuator_01.csv'
ppath = str(ir.files('fonsim').joinpath(filepath))
system.add(fons.FreeActuator('actu_0', fluid=fluid, curve=ppath))
system.add(fons.FreeActuator('actu_1', fluid=fluid, curve=ppath))
system.add(fons.CircularTube('tube_0', fluid=fluid, diameter=2e-3, length=1.20))
system.add(fons.CircularTube('tube_1', fluid=fluid, diameter=2e-3, length=0.60))
system.connect('source', 'tube_0')
system.connect('tube_0', 'actu_0')
system.connect('actu_0', 'tube_1')
system.connect('tube_1', 'actu_1')

# Create and run simulation
sim = fons.Simulation(system, duration=9, step_max=1e-2)
sim.run()

# Plot simulation results
fig, axs = plt.subplots(3, sharex=True)
fons.plot(axs[0], sim, 'pressure', 'bar', ('source', 'actu_0', 'actu_1'))
axs[0].set_ylim(1.1, 2.0)
fons.plot_state(axs[1], sim, 'mass', 'g', ['actu_0', 'actu_1'])
fons.plot(axs[2], sim, 'massflow', 'g/s', ['tube_0', 'tube_1'])
for a in axs: a.legend()
plt.show(block=False)

# Also do a parametric plot
rho = fluid.rho if hasattr(fluid, 'rho') else fluid.rho_stp
p_atm = fons.pressure_atmospheric
m_a0 = system.get('actu_0').get_state('mass')
p_a0 = system.get('actu_0').get('pressure')
v_a0 = m_a0 / rho * p_atm / p_a0
m_a1 = system.get('actu_1').get_state('mass')
p_a1 = system.get('actu_1').get('pressure')
v_a1 = m_a1 / rho * p_atm / p_a1

fig, ax = plt.subplots(1)
ax.plot(v_a0 * 1e6, v_a1 * 1e6)
ax.set_aspect('equal')
ax.set_xlabel('volume actuator 0 [ml]')
ax.set_ylabel('volume actuator 1 [ml]')
plt.show()
