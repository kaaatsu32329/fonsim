"""
Example 01
A very simple system

src -=- container

2020, July 22
"""

# Import fonsim package, for building and simulating a fluidic system
import fonsim as fons
# Import matplotlib package, for plotting the simulation results
import matplotlib.pyplot as plt


# Create a new pneumatic system
system = fons.System()

# Choose a fluid
fluid = fons.air

# Create components and add them to the system
# Note: for now, only take major (pipe friction) losses into account.
mycomponent = fons.PressureSource("source_00", pressure=2e5)
system += mycomponent
system += fons.Container("container_00", fluid=fluid, volume=50e-6)
system += fons.CircularTube("tube_00", fluid=fluid, diameter=2e-3, length=0.60)

# Connect the components to each other.
system.connect("tube_00", "source_00")
system.connect("tube_00", "container_00")

# Let's simulate the system!
sim = fons.Simulation(system, duration=0.3)
# Run the simulation
sim.run()


# Plot simulation results
# For matplotlib tutorials, see
# https://docs.python.org/3/library/copy.html

# Manual method
fig, axs = plt.subplots(3, sharex=True)
fig.suptitle("Simulation results")
axs[0].plot(sim.times, system.get("source_00").get('pressure')*1e-5, label='source_00')
axs[0].plot(sim.times, system.get("container_00").get('pressure')*1e-5, label='container_00')
axs[0].set_ylabel('pressure [bar]')
axs[1].plot(sim.times, system.get("container_00").get_state('mass')*1e3, label='container_00')
axs[1].set_ylabel('mass [g]')
axs[2].plot(sim.times, system.get("source_00").get('massflow')*1e3, label='source_00')
axs[2].plot(sim.times, system.get("container_00").get('massflow')*1e3, label='container_00')
axs[2].set_ylabel('mass flow [g/s]')
axs[2].set_xlabel('time [s]')
for a in axs: a.legend()
plt.show(block=False)

# Automatic method
fig, axs = plt.subplots(3, sharex=True)
fig.suptitle("Simulation results")
fons.plot(axs[0], sim, 'pressure', unit='bar', components=('source_00', 'container_00'))
fons.plot_state(axs[1], sim, 'mass', unit='g', components=('container_00',))
fons.plot(axs[2], sim, 'massflow', unit='g/s', components=('source_00', 'container_00'))
axs[2].set_xlabel('time [s]')
plt.show()
