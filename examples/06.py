"""
2022, May 29
"""

import matplotlib.pyplot as plt
import importlib_resources as ir
import fonsim as fons

# Load example pvcurve CSV file
filepath = 'resources/Measurement_60mm_balloon_actuator_01.csv'
# Use importlib_resources and load as bytestring
# because above file resides in the FONSim package.
# Not necessary for normal files, there one can simply provide the filepath
# as argument for the parameter `data` when initializing the PVCurve object.
ppath = str(ir.files('fonsim').joinpath(filepath))

# Load pvcurve in PVCurve object
pvcurve = fons.data.PVCurve(data=ppath, pressure_reference='relative')

# Plot the curve
fig, ax = plt.subplots(1)
ax.plot(pvcurve.v * 1e6, pvcurve.p * 1e-5)
ax.set_ylabel('absolute pressure [bar]')
ax.set_xlabel('volume [ml]')
plt.show()
