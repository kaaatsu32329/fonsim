"""
Example 04
Based on: 03
A soft robotics balloon actuator with pvcurves.

src -=- act

2020, July 22
"""

import fonsim as fons
import matplotlib.pyplot as plt
import importlib_resources as ir

# Pressure reference
waves = [(0, 0.890), (1, 0.650), (2, 0.550), (3, 0.650)]
wave_function = fons.wave.Custom(waves)*1e5 + fons.pressure_atmospheric

# Select a fluid
# Previous examples required a compressible fluid.
# This one doesn't, feel free to try with an incompressible fluid (e.g. water)!
fluid = fons.air

# Create system
# The FreeActuator allows to simulate components characterized by a pvcurve.
# The curve argument can take a pvcurve specified as a CSV file.
system = fons.System()
system.add(fons.PressureSource('source', pressure=wave_function))
filepath = 'resources/Measurement_60mm_balloon_actuator_01.csv'
ppath = str(ir.files('fonsim').joinpath(filepath))
system.add(fons.FreeActuator('actu', fluid=fluid, curve=ppath))
system.add(fons.CircularTube('tube', fluid=fluid, diameter=2e-3, length=0.60))
system.connect('tube', 'source')
system.connect('tube', 'actu')

# Create and run simulation
sim = fons.Simulation(system, duration=4)
sim.run()

# Plot simulation results
fig, axs = plt.subplots(3, sharex=True)
fons.plot(axs[0], sim, 'pressure', 'bar', ('source', 'actu'))
axs[0].set_ylim(1.4, 2.0)
fons.plot_state(axs[1], sim, 'mass', 'g', ['actu'])
fons.plot(axs[2], sim, 'massflow', 'g/s', ['tube'])
for a in axs: a.legend()
plt.show()
